import config from '../utils/config';
import { User, UserRole, managerUser, normalUser, Research, research } from '../utils/data';
import { clickSidebarLink, createAccountType, deleteResearch, deleteUser, filterResearchBy, login, loginAs, logout, openDashboard, openSidebar, verifyDownload } from '../utils/utils';
const Papa = require('papaparse');

function openUserEditPage(user: User) {
  openDashboard();
  clickSidebarLink('Manage Users')
  // wait for users to load
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  // filter by users
  cy.get('.vx-card__body .v-select [role="combobox"] [type="search"]').first().click()
  let roleOptions: { [role in UserRole]: number } = {
    // all: 1,
    admin: 2,
    user: 3,
    editor: 4
  }
  cy.get(`.vx-card__body .v-select [role="listbox"] [role="option"]:nth-child(${roleOptions[user.role]})`).click()
  // filter by username
  cy.get('#page-user-list > div.vx-card.p-6 > div.ag-theme-material.w-100.my-4.ag-grid-table > div > div.ag-root-wrapper-body.ag-layout-normal > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div:nth-child(2) > div:nth-child(2) > div.ag-floating-filter-body > div > input').click()
  cy.get('.ag-header-cell:nth-child(2) .ag-floating-filter-input').type(user.username)
  cy.wait(config.filterRefreshWaitTime);
  // wait for users to load
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  // open edit page of first user
  cy.get('#page-user-list > div.vx-card.p-6 > div.ag-theme-material.w-100.my-4.ag-grid-table > div > div.ag-root-wrapper-body.ag-layout-normal > div > div.ag-body-viewport.ag-layout-normal.ag-row-animation > div.ag-center-cols-clipper > div > div > div.ag-row.ag-row-no-focus.ag-row-even.ag-row-level-0.ag-row-position-absolute a').first().click()
  cy.url({ timeout: config.pageLoadTimeout }).should('contain', '/user/user-view/');
  // wait for edit form to load
  cy.get('.content-wrapper input').should('be.visible');
}

function testEditUser(user: User) {
  openUserEditPage(user);
  // TODO: Actually edit user details once form is implemented by developer
}

function assignUserRight(user: User, right: 'upload' | 'download' | 'generateReport' | 'generateReportForOther') {
  openUserEditPage(user);
  // TODO: Actually assign rights once developer fixes edit page
}

describe('A - Administrator', () => {
  before(() => {
    loginAs('admin');
  });
  it('A1 - can login', () => { });
  it('A2 - can create user accounts', () => {
    createAccountType(normalUser);
  })
  it('A3 - can manage user accounts', () => {
    loginAs('admin');
    testEditUser(normalUser);
  })
  it('A6 - can assign upload rights to users', () => {
    assignUserRight(normalUser, 'upload');
  })
  it('A7 - can assign download rights to users', () => {
    assignUserRight(normalUser, 'download');
  })
  it('A10 - can delete users', () => {
    deleteUser(normalUser.username);
  })
  it('A4 - can create manager accounts', () => {
    createAccountType(managerUser);
  })
  it('A5 - can manage manager accounts', () => {
    loginAs('admin');
    testEditUser(managerUser);
  })
  it('A8 - can assign report generation rights to managers', () => {
    assignUserRight(managerUser, 'generateReport');
  })
  it('A9 - can assign report generation for other researchers rights to managers', () => {
    assignUserRight(managerUser, 'generateReportForOther');
  })
  it('A11 - can delete manager users', () => {
    deleteUser(managerUser.username);
  })
  it('A12 - can generate overall system reports', () => {
    openDashboard();

    const reportName = 'test';
    cy.get('body > div.vs-component.con-vs-dialog.export-options.vs-dialog-primary > div.vs-dialog > div > div.vs-component.vs-con-input-label.vs-input.w-full.vs-input-primary > div > input').click()
    cy.get('body > div.vs-component.con-vs-dialog.export-options.vs-dialog-primary > div.vs-dialog > div > div.vs-component.vs-con-input-label.vs-input.w-full.vs-input-primary > div > input').type(reportName)
    cy.get('body > div.vs-component.con-vs-dialog.export-options.vs-dialog-primary > div.vs-dialog > div > div.v-select.my-4.vs--single.vs--searchable > div').click()
    cy.get('body > div.vs-component.con-vs-dialog.export-options.vs-dialog-primary > div.vs-dialog > div > div.v-select.my-4.vs--open.vs--single.vs--searchable > ul > li.vs__dropdown-option.vs__dropdown-option--highlight').click()
    cy.get('body > div.vs-component.con-vs-dialog.export-options.vs-dialog-primary > div.vs-dialog > footer > button.vs-component.vs-button.vs-dialog-accept-button.vs-button-primary.vs-button-filled').click()

    // TODO: Verify downloaded CSV once the developer actually implements it
  })
  after(() => {
    logout();
  });
});

function generateReports(testId: string) {
  describe(`${testId} - can draw reports by:`, () => {
    it('output type', () => {
      filterResearchBy('PublicationType', 'book chapter');
    });
    it('contributor', () => {
      expect(true, 'no option to filter by contributor').to.eq(false);
    });
    it('author count', () => {
      expect(true, 'no option to filter by author count').to.eq(false);
    });
    it('SDG', () => {
      filterResearchBy('SDG Goals', 'no poverty');
    });
    it('quarter', () => {
      filterResearchBy('publicationDate', 'Q1');
    });
    it('outlet', () => {
      // TODO: What is an outlet?
      expect(true, 'no option to filter by outlet').to.eq(false);
    });
  });
}

describe('B - Manager', () => {
  before(() => {
    loginAs('manager');
  });
  it('B1 - can login', () => { });
  it('B2 - can view publications', () => {
    clickSidebarLink('Manage Research');
    filterResearchBy('Author', 'tinashe');
    filterResearchBy('Author', 'andre');
  });
  generateReports('B3');
  it('B4 - can generate PDF and CSV reports', () => {
    clickSidebarLink('Manage Research');

    cy.get('#page-user-list').contains('Export as CSV').click();
    verifyDownload('export.csv').then((csv) => {
      const { data } = Papa.parse(csv);
      expect(data[0], 'csv must have correct headers').to.eql([
        "\ufeff\"Author\"",
        "Title",
        "PublicationType",
        "publicationDate",
        "SDG Goals",
        "apaStyle",
        "mainSupervisor",
        "coSupervisor",
        "level",
        "conferencePaper",
        "Actions"
      ]);
      expect(data.length, 'csv must have more than 1 row').to.greaterThan(1);
    });

    cy.get('#page-user-list').contains('Actions').click();
    cy.get('.vs-dropdown--menu').contains('Print').click();
    verifyDownload('export.pdf').should('exist');
  });
});

function downloadResearch(research: Research) {
  openDashboard();
  cy.wait(2000);
  cy.get('.vs-table--search-input:not([disabled])').type(research.Title);
  cy.wait(config.filterRefreshWaitTime);
  // select first row
  cy.get('tbody > :nth-child(1) > .td-check').first().click();
  // click download
  cy.get('.is-selected > .whitespace-no-wrap > :nth-child(1) > :nth-child(3) > .feather').click();
  verifyDownload(research.file).should('exist');
}

describe('C - Authors/Owners/Researchers', () => {
  before(() => {
    loginAs('researcher');
  });
  it('C1 - can login', () => { });
  describe('C2 - can create research', () => {
    it('C2 - with basic information', () => {
      clickSidebarLink('Upload Research');

      cy.get('#Fulldetails0 > .vx-row > :nth-child(2) > .vs-component > .vs-con-input > .vs-inputx').type(research.Title);
      cy.get('#Fulldetails0 > .vx-row > :nth-child(6) > .con-select > .input-select-con > .input-select').click();
      cy.get(`[data-text="${research['SDG Goals']}"] > .vs-select--item`).click();
      cy.get('#Fulldetails0 > .vx-row > :nth-child(3) > .vs-component > .vs-con-input > .vs-inputx').type(research.apaStyle)
      cy.get('.vs-textarea').type(research.abstract);
    });

    it('C2.1 - with type of publication', () => {
      const types = [
        'Post graduate supervision',
        'Peer Reviewed Conference publications',
        'Oral/Conference presentations',
        research.PublicationType,
      ]
      types.forEach((type) => {
        cy.get(':nth-child(1) > .con-select > .input-select-con > .input-select').click();
        cy.get(`[data-text="${type}"] > .vs-select--item`).click();
      })

      cy.get('.wizard-btn').click();
    });


    it('C2.2 - with details of co-authorship, collaboration and supported SGDs', () => {
      cy.get('#Co-authorship1 > .vx-row > :nth-child(1) > .vs-component > .vs-con-input > .vs-inputx').type(research.coAuthors)
      cy.get('#Co-authorship1 > .vx-row > :nth-child(2) > .vs-component > .vs-con-input > .vs-inputx').type(research.mainSupervisor)
      cy.get(':nth-child(4) > .con-select > .input-select-con > .input-select').click();
      cy.get(`[data-text="${research.level}"] > .vs-select--item`).click();
      cy.get('#Co-authorship1 > .vx-row > :nth-child(6) > .con-select > .input-select-con > .input-select').click()
      cy.get(`[data-text="${research.faculty}"] > .vs-select--item`).click();

      cy.get('.wizard-footer-right > span > .wizard-btn').click();
    });

    it('C2.3 - with publication date', () => {
      cy.get('#Whenpublished2 > .vx-row > :nth-child(1) > .vs-component > .vs-con-input > .vs-inputx').type(research.publicationDate);
    });

    it('C2.4 - with research document', () => {
      cy.fixture(research.file).then((fileContent) => {
        cy.get('#file').selectFile({
          fileName: research.file,
          contents: Cypress.Buffer.from(fileContent),
          mimeType: 'application/pdf',
        });
      });
    })

    it('C2.5 - and upload it', () => {
      cy.get('.wizard-footer-right > span > .wizard-btn').click();

      // wait for the research to finish uploading
      cy.get('.con-vs-loading', { timeout: config.pageLoadTimeout }).should('not.exist');

      clickSidebarLink('Upload Research');
      filterResearchBy('Title', research.Title);
      deleteResearch('Title', research.Title, false);
    });
  });

  it('C3 - can download research', () => {
    downloadResearch(research);
  });

  generateReports('C4');
});

describe('D - Public users', () => {
  before(() => {
    loginAs('user');
  });
  it('D1 - can login', () => { });
  it('D2 - can view research outputs', () => {
    openDashboard();
    cy.get('.research-container tr.tr-values:nth-child(1)').should('be.visible');
  });
  it('D3 - can search research outputs', () => {
    openDashboard();
    filterResearchBy('Title', research.Title);
  });
  it('D4 - filter research outputs', () => {
    openDashboard();
    filterResearchBy('Author', 'Tinashe');
  });
  it('D5 - can download research outputs', () => {
    openDashboard();
    downloadResearch(research);
  });
})