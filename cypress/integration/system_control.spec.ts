import { normalUser } from "../utils/data";
import { getSidebarLink, loginAs, openSidebar, createAccountType, deleteUser } from "../utils/utils";

function ensureLinksNotVisible(links: string[]) {
  links.forEach(link => {
    getSidebarLink(link).should('not.exist');
  });
}

describe('F - System control', () => {
  it('F1 - users have unique IDs', () => {
    loginAs('admin');
    createAccountType(normalUser);
    createAccountType({ ...normalUser, password: 'cakeisnice' }, false);
    deleteUser(normalUser.username);
  })
  it('F2 - admin functions should be separate from normal user functions', () => {
    loginAs('admin');
    openSidebar();
    // make sure separator exists
    cy.get('.vs-sidebar--items .navigation-header').eq(1).should('exist').should('contain.text', 'Settings');
    cy.get('.vs-sidebar--item').then($elements => {
      const strings = [...$elements].map(el => el.innerText)
      expect(strings).to.deep.equal([
        "admin",
        "user",
        "Create User",
        "Upload Research",
        "Manage Users",
        "Manage Research",
        "User Settings",
        "Logout"
      ])
    })
  });
  describe('F3 - prevent clicking menu options not assigned to user', () => {
    const adminLinks = [
      'Create User',
      'Manage Reports',
      'System Reports',
    ];
    const researcherLinks = [
      'Upload Research',
      'Manage Research',
    ];

    it('manager', () => {
      loginAs('manager');
      openSidebar();
      ensureLinksNotVisible(adminLinks);
    });

    it('researcher', () => {
      loginAs('researcher', true);
      openSidebar();
      ensureLinksNotVisible(adminLinks);
    });

    it('user', () => {
      loginAs('user', true);
      openSidebar();
      ensureLinksNotVisible(adminLinks);
      ensureLinksNotVisible(researcherLinks);
    });
  })
});