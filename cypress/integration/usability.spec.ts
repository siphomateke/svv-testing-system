import config from "../utils/config";
import { clickSidebarLink, deleteResearch, deleteUser, loginAs, verifyDownload } from "../utils/utils";

function auditUrl(url: string) {
  cy.visit(url, { timeout: config.firstLoadTimeout });
  cy.lighthouse(
    {
      // performance: 85,
      accessibility: 100,
      'best-practices': 100,
    },
    {
      onlyCategories: ['accessibility', 'best-practices'],
      formFactor: "desktop",
      screenEmulation: { disabled: true },
      output: 'html'
    }
  );
}

describe("G - Usability", { taskTimeout: 90000 }, () => {
  it("G1 - user can view manual", () => {
    loginAs("user");
    clickSidebarLink("Manual");
    verifyDownload("manual.pdf").should("exist");
  });
  it("G2 - user can undo actions", () => {
    loginAs('admin');

    deleteUser('john_smith');
    cy.get('.undo-toast').should('exist');

    deleteResearch('Title', 'study');
    cy.get('.undo-toast').should('exist');
  });
  it("G3 - user confirmation is requested", () => {
    loginAs('admin');

    deleteUser('john_smith');
    cy.get('.confirm-modal').should('exist');

    deleteResearch('Title', 'study');
    cy.get('.confirm-modal').should('exist');
  });
  describe("G4 & G5 - website is accessible and follows best practices", () => {
    it('login', () => auditUrl("/pages/login"));
    it('user settings', () => auditUrl('/pages/user-settings'));

    it('login as admin', () => loginAs('admin'));

    it('admin dashboard', () => auditUrl('/dashboard/admin'));
    it('create user', () => auditUrl('/admin/user/create'));
    it('manage users', () => auditUrl('/apps/user/user-list'));
    it('manage research', () => auditUrl('/apps/research/research-list'));

    it('login as manager', () => loginAs('manager'));

    it('manager dashboard', () => auditUrl('/dashboard/manager'));
    it('upload research', () => auditUrl('/manager/research/create'));
    it('my research', () => auditUrl('/apps/manager/research-list'));

    it('login as user', () => loginAs('user'));

    it('user dashboard', () => auditUrl('/dashboard/user'));
  });
});
