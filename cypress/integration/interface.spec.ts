import config from "../utils/config";
import { loginAs, openSidebar } from "../utils/utils";

describe('E - User interface', () => {
  it('E1 - logged-out users see login', () => {
    cy.visit('/', { timeout: config.firstLoadTimeout });
    cy.url({ timeout: config.firstLoadTimeout }).should('contain', `/pages/login`);
  })

  describe('users can only see their links', () => {
    function checkLinks(links: string[]) {
      openSidebar();
      links.forEach(link => {
        cy.get('.vs-sidebar--items').contains(link);
      })
    }

    it('E2 - all users', () => {
      loginAs('admin');
      checkLinks(['User Settings', 'Logout']);
    });

    it('E2.1 - admins', () => {
      loginAs('admin');
      checkLinks([
        'Create User',
        'Manage Reports',
        'System Reports',
      ]);
    });

    it('E2.2 - managers', () => {
      loginAs('manager');
      checkLinks(['Reports']);
    });

    it('E2.3 - researcher', () => {
      loginAs('researcher');
      checkLinks([
        'Add Research',
        'View Research',
        'Reports'
      ]);
    });
  });
});