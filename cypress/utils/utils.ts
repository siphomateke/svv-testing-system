import config from "./config";
import { ResearchColumn, User } from "./data";
const path = require("path");

export function logout() {
  // cy.visit();
  // openSidebar();
  // // click logout
  // cy.get('div.vs-sidebar--item:nth-child(7) > a:nth-child(1)').click();
  cy.visit('/pages/login?logout=1', { timeout: config.pageLoadTimeout });
}


export function login(username, password, userType, logoutFirst = false) {
  if (!logoutFirst) {
    cy.visit("/pages/login", { timeout: config.firstLoadTimeout });
  } else {
    logout();
  }

  cy.get('[name="username"]').clear().type(username).should('have.value', username);
  cy.get('[name="password"]').clear().type(password).should('have.value', password);

  cy.get('.vs-button-filled').click();

  // make sure there are no login errors
  cy.get('span.text-danger:nth-child(4)').should('be.empty');

  cy.url({ timeout: config.pageLoadTimeout }).should('contain', `/dashboard/${userType}`);
  // cy.get('.font-semibold').should('have.text', 'Andre Braulio');
}

export function loginAs(userType: 'admin' | 'manager' | 'user' | 'researcher', logoutFirst = false) {
  let username;
  let password = '1234';
  if (userType === 'admin') {
    username = '219034559';
  } else if (userType === 'manager') {
    username = 'tinashe';
  } else if (userType === 'user') {
    username = 'rianne';
  } else if (userType === 'researcher') {
    // FIXME: Login as researcher
    username = 'tinashe';
    userType = 'manager';
  }
  login(username, password, userType, logoutFirst);
}

export function openSidebar() {
  cy.get('#content-area > div.relative > div > header > div.vs-con-items > span').click({ force: true });
}

export function getSidebarLink(name) {
  openSidebar();
  return cy.get('.vs-sidebar--items').contains(name);
}

export function clickSidebarLink(name) {
  const link = getSidebarLink(name);
  link.then((data) => {
    const el = data[0];
    if (el instanceof HTMLAnchorElement) {
      link.click();
      cy.get('.vs-sidebar--background').click({ force: true, multiple: true });
      cy.url({ timeout: config.pageLoadTimeout }).should('contain', el.href);
    }
  });
  // TODO: Consider waiting for URL of sidebar link to load
  // cy.url({ timeout: config.pageLoadTimeout }).should('contain', `/apps/manager/research-list`);
}

export function openDashboard() {
  openSidebar();
  cy.get('.vs-sidebar-group').click();
  cy.get('.vs-sidebar--item:nth-child(1) > a:nth-child(1)').first().click();
  cy.get('.vs-sidebar--background').click({ force: true, multiple: true });
  cy.url({ timeout: config.pageLoadTimeout }).should('contain', '/dashboard');
}

export function verifyDownload(filename: string) {
  const downloadsFolder = Cypress.config('downloadsFolder')
  const downloadedFilename = path.join(downloadsFolder, filename)
  return cy.readFile(downloadedFilename);
}

export function deleteUser(username: string, confirmDeletion = true) {
  openDashboard();
  clickSidebarLink('Manage Users');

  // wait for users to load
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  // filter list by username
  cy.get('#page-user-list > div.vx-card.p-6 > div.ag-theme-material.w-100.my-4.ag-grid-table > div > div.ag-root-wrapper-body.ag-layout-normal > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div:nth-child(2) > div:nth-child(2) > div.ag-floating-filter-body > div > input').click()
  cy.get('.ag-header-cell:nth-child(2) .ag-floating-filter-input').type(username)
  cy.wait(config.filterRefreshWaitTime);
  // click first match
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  cy.get('.ag-header-cell:nth-child(1) > .ag-header-select-all .ag-icon').click()
  // open actions dropdown
  cy.get('#page-user-list > div.vx-card.p-6 > div.flex.flex-wrap.items-center > button').click()
  // click delete
  cy.get('body > div.con-vs-dropdown--menu.vs-dropdown-menu > ul > li:nth-child(1) > a').click()
  if (confirmDeletion) {
    // return to manage users page
    openDashboard();
    openSidebar();
    clickSidebarLink('Manage Users');
    // wait for users to load
    cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
    // filter list by username
    cy.get('#page-user-list > div.vx-card.p-6 > div.ag-theme-material.w-100.my-4.ag-grid-table > div > div.ag-root-wrapper-body.ag-layout-normal > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div:nth-child(2) > div:nth-child(2) > div.ag-floating-filter-body > div > input').click()
    cy.get('.ag-header-cell:nth-child(2) .ag-floating-filter-input').type(username)
    cy.wait(config.filterRefreshWaitTime);
    // make sure the user no longer exists
    cy.get('.ag-center-cols-container > .ag-row').should('not.exist')
  }
}

export function createAccountType(user: User, mustExist = true) {
  clickSidebarLink('Create User');

  const form = '#content-area > div.content-wrapper > div > div > div > div.vx-row > div > div > div.vx-card__collapsible-content.vs-con-loading__container > div';
  cy.get(`${form} > div:nth-child(1) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.firstName);
  cy.get(`${form} > div:nth-child(2) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.lastName);
  cy.get(`${form} > div:nth-child(3) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.username);
  cy.get(`${form} > div:nth-child(4) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.password);
  cy.get(`${form} > div:nth-child(5) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.password);
  cy.get('div.input-select-con:nth-child(2) > input:nth-child(1)').type(user.role, { force: true });
  cy.get(`${form} > div:nth-child(7) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.dob);
  cy.get(`${form} > div:nth-child(8) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.department);
  cy.get(`${form} > div:nth-child(9) > div > div > div > input`).click();
  cy.get('.isFocus .vs-inputx').type(user.faculty);
  // submit
  cy.get('.mr-3 > .vs-button-text').click();

  // make sure user now exists
  if (mustExist) {
    cy.get('.con-text-noti > p:nth-child(2)').should('not.contain.text', 'Error');
    login(user.username, user.password, user.role, true);
    cy.get('#page-login > div > div > div.vx-card__collapsible-content.vs-con-loading__container > div > div > div.vx-col.sm\\:w-full.md\\:w-full.lg\\:w-1\\/2.d-theme-dark-bg > div > div.con-vs-tabs.vs-tabs.vs-tabs-primary.vs-tabs-position-top > div.con-slot-tabs > div > div > span:nth-child(4)').should('be.empty');
    cy.get('.font-semibold').should('have.text', `${user.firstName} ${user.lastName}`);
  } else {
    cy.get('.con-text-noti > p:nth-child(2)').should('contain.text', 'Error');
  }
}

export function filterResearchBy(column: ResearchColumn, value: string, mustExist = true) {
  cy.url({ timeout: 0 }).should('contain', 'research')

  // Reset search filters
  cy.get('.vx-card__action-buttons > :nth-child(2) > .feather').click();
  // Find column by header name
  const child = cy.get('.ag-header-container').contains(column, { matchCase: false }).closest('.ag-header-cell');
  return child.invoke('index').then((index) => {
    cy.get(`.ag-header-container .ag-header-row:nth-child(2) .ag-header-cell:nth-child(${index + 1}) input`).type(value);
    cy.wait(config.filterRefreshWaitTime);
    if (mustExist) {
      cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
    } else {
      cy.get('.ag-center-cols-container > .ag-row').should('not.be.visible');
    }
  });
}

export function deleteResearch(column: ResearchColumn, value: string, goToResearchPage = true) {
  if (goToResearchPage) {
    openDashboard();
    clickSidebarLink('My Research');
  }

  // wait for research to load
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  filterResearchBy(column, value);
  // click first match
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  cy.get('.ag-header-cell:nth-child(1) > .ag-header-select-all .ag-icon').click()
  // open actions dropdown
  cy.get('#page-user-list > div.vx-card.p-6 > div.flex.flex-wrap.items-center > button').click()
  // click delete
  cy.get('body > div.con-vs-dropdown--menu.vs-dropdown-menu > ul > li:nth-child(1) > a').click()
  openDashboard();
  clickSidebarLink('My Research');
  // wait for research to load
  cy.get('.ag-center-cols-container > .ag-row').should('be.visible');
  filterResearchBy(column, value, false);
}