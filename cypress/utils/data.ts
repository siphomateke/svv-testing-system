export type UserRole = 'user' | 'editor' | 'admin';

export interface User {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  dob: string;
  department: string;
  faculty: string;
  role: UserRole;
}

export const normalUser: User = {
  firstName: 'John',
  lastName: 'Smith',
  username: 'john_smith',
  password: 'bowtiesarecool',
  dob: '1995-11-05',
  department: 'Computer Science',
  faculty: 'Computing & Informatics',
  role: 'user'
};

export const managerUser: User = {
  firstName: 'River',
  lastName: 'Song',
  username: 'riversong',
  password: 'spoilers',
  dob: '2000-01-01',
  department: 'Computer Science',
  faculty: 'Computing & Informatics',
  role: 'editor'
};

export type ResearchColumn =
  'Author'
  | 'Title'
  | 'PublicationType'
  | 'publicationDate'
  | 'SDG Goals'
  | 'apaStyle'
  | 'mainSupervisor'
  | 'coSupervisor'
  | 'level'
  | 'conferencePaper'
  | 'Actions';

export type Research = { [key in ResearchColumn]?: string } & {
  abstract: string;
  coAuthors: string;
  faculty: string;
  file: string;
}

export const research: Research = {
  Title: 'Genetic Algorithms and Neural Networks (GANN)',
  'SDG Goals': '4 (Quality Education)',
  apaStyle: 'Kaviani, Mahshid & MirRokni, Seyed Majid. (2017). Applying Genetic Algorithm in Architecture and Neural Network Training. 118.',
  abstract: 'A genetic algorithm approach is presented and compared with the gradient descent in failure rate and time to obtain a solution.',
  PublicationType: 'Post graduate supervision',
  coAuthors: 'Kapuire',
  mainSupervisor: 'Kandjimi',
  level: 'Masters',
  faculty: 'Computing & Informatics',
  publicationDate: '2020-12-25',
  file: 'Genetic Algorithms and Neural Networks (GANN).pdf',
};