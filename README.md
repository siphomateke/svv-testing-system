# SVV Testing System

Tests the [NUST Research Management System](https://roms1.herokuapp.com/) using Cypress tests.

![Example test run output](./images/cypress.png)

Tests are divided into spec files located in `cypress/integration/`. Additional data required for the tests can be found in `cypress/utils`.

## Installation

1. Download this repository
2. [Install Node.js](https://nodejs.org/en/download/)
3. [Install pnpm](https://pnpm.io/installation)
4. Install dependencies

  ```bash
  pnpm install
  ```

## Usage

The easiest way to run the tests is using the Cypress GUI:

```bash
pnpm run cypress:open
```

Simply select the browser to run the tests in (Firefox works best except for the usability tests), and click a test spec to run. Then observe the results in the window that opens.

For more comprehensive testing, tests can be run and recorded with screenshots and videos uploaded to Cypress.io when necessary.

```bash
pnpm run cypress:run
````

> Note: You will need to provide a valid cypress key to upload the results. E.g. `pnpm run cpyress:run --key csa1212-321dsads1-23121-32132`.
